﻿using aejw.Network;
using QubeUtils;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace iSky
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
            try
            {
                if (Settings.GetString("ExportLocation").Substring(0, 2) == "\\\\")
                {
                    NetworkDrive oNetDrive = new NetworkDrive();

                    oNetDrive.Force = true;
                    oNetDrive.Persistent = true;
                    oNetDrive.LocalDrive = "T:";
                    oNetDrive.ShareName = Settings.GetString("ExportLocation");
                    oNetDrive.MapDrive(Settings.GetString("ExportUser"), Settings.GetString("ExportPassword"));

                    oNetDrive = null;

                }

                ServiceBase[] ServicesToRun;
                ServicesToRun = new ServiceBase[]
                {
                    new iSkyService(args)
                };
                ServiceBase.Run(ServicesToRun);
            }
            catch (Exception ex)
            {
                string SourceName = "WindowsService.ExceptionLog";
                if (!EventLog.SourceExists(SourceName))
                {
                    EventLog.CreateEventSource(SourceName, "Application");
                }

                EventLog eventLog = new EventLog();
                eventLog.Source = SourceName;
                string message = string.Format("Exception: {0} \n\nStack: {1}", ex.Message, ex.StackTrace);
                eventLog.WriteEntry(message, EventLogEntryType.Error);
            }
        }
    }
}
