﻿using QubeUtils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iSky
{
    public class iSkyRecord : MapsObject
    {
        public string DealerCode { get; set; }
        public string CentreName { get; set; }
        public string CentreAddress1 { get; set; }
        public string CentreAddress4 { get; set; }
        public string CentreEmail { get; set; }
        public string TransactionType { get; set; }
        public DateTime InvoiceDate { get; set; }
        public string CustomerMagicNumber { get; set; }
        public string Title { get; set; }
        public string Initials { get; set; }
        public string FirstName { get; set; }
        public string Surname { get; set; }
        public string EmailAddress { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string Address4 { get; set; }
        public string Address5 { get; set; }
        public string PostCode { get; set; }
        public string HomeTelephoneNumber { get; set; }
        public string DayTelephoneNumber { get; set; }
        public string MobileNumber { get; set; }
        public string FaxNumber { get; set; }
        public string CompanyName { get; set; }
        public string CompanyAddress1 { get; set; }
        public string CompanyAddress2 { get; set; }
        public string CompanyAddress3 { get; set; }
        public string CompanyAddress4 { get; set; }
        public string CompanyAddress5 { get; set; }
        public string CompanyPostCode { get; set; }
        public string StandardModel { get; set; }
        public string RegNumber { get; set; }
        public string ChassisNumber { get; set; }
        public string RangeName { get; set; }
        public string KatashikiModel { get; set; }
        public DateTime RegDate { get; set; }
        public string iSKYModelId { get; set; }
        public decimal TotalInvoiceValue { get; set; }
        public bool APIRan { get; set; }
        public bool Consent { get; set; }
        public int Mileage { get; set; }
        public bool HasWarranty { get; set; }
        public string Manufacturer { get; set; }

        public override List<string> notMapped => new List<string>() { "Consent" };
    }
}
