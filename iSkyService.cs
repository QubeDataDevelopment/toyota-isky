﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Threading.Tasks;
using QubeUtils;
using QubeUtils.WebApi.ConsentApi;
using System.Data.SqlClient;
using System.Configuration;
using System.Runtime.InteropServices;
using System.Threading;
using System.Xml;
using System.IO;
using Newtonsoft.Json;

namespace iSky
{
    public partial class iSkyService : ServiceBase
    {
        private System.Diagnostics.EventLog eventLog1;
        private int _eventId = 1;
        private int eventId
        {
            get
            {
                if (_eventId >= 65534)
                    _eventId = 0;

                return _eventId;
            }
            set
            {
                _eventId = value;
            }
        }
        private System.Timers.Timer timer = new System.Timers.Timer();
        private System.Timers.Timer activityTimer = new System.Timers.Timer();
        private SemaphoreSlim throttler = new SemaphoreSlim(initialCount: Settings.GetInt("ThreadCount"));
        private static string connStrName = Settings.GetString("ConnectionStringNameVCL");
        private static string connStr = ConfigurationManager.ConnectionStrings[connStrName].ToString();
        private string exportLocation = Settings.GetString("ExportLocation").Substring(0, 2) == "\\\\" ? "T:" : Settings.GetString("ExportLocation");
        private bool running = false;
        private DateTime now = DateTime.Now;

        public iSkyService(string[] args)
        {
            InitializeComponent();

            string eventSourceName = "MySource";
            string logName = "MyNewLog";
            if (args.Count() > 0)
            {
                eventSourceName = args[0];
            }
            if (args.Count() > 1)
            {
                logName = args[1];
            }
            eventLog1 = new System.Diagnostics.EventLog();
            if (!System.Diagnostics.EventLog.SourceExists(eventSourceName))
            {
                System.Diagnostics.EventLog.CreateEventSource(eventSourceName, logName);
            }
            eventLog1.Source = eventSourceName;
            eventLog1.Log = logName;
        }

        protected override void OnStart(string[] args)
        {
            // Update the service state to Start Pending.  
            ServiceStatus serviceStatus = new ServiceStatus();
            serviceStatus.dwCurrentState = ServiceState.SERVICE_START_PENDING;
            serviceStatus.dwWaitHint = 120000;
            SetServiceStatus(this.ServiceHandle, ref serviceStatus);

            //eventLog1.WriteEntry("iSky Service started!", EventLogEntryType.Information, eventId++);

            StartActivityTimer();

            // Main timer setup
            StartTimer(Settings.GetBool("ForceStart"));

            // Update the service state to Running.  
            serviceStatus.dwCurrentState = ServiceState.SERVICE_RUNNING;
            SetServiceStatus(this.ServiceHandle, ref serviceStatus);
        }

        private void StartActivityTimer()
        {
            ActivityTimer_Elapsed(null, null);

            activityTimer.Interval = TimeSpan.FromMinutes(5).TotalMilliseconds;
            activityTimer.Elapsed += ActivityTimer_Elapsed;
            activityTimer.Start();
        }

        private void ActivityTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            MSSQLHelper.ExecuteSQL("INSERT INTO ActivityLogSystem(TimeStamp, Activity) VALUES(GETDATE(), 'AUTOLOAD : iSKY is live.')", null);
        }

        protected override void OnStop()
        {
            eventLog1.WriteEntry("iSky Service stopped!", EventLogEntryType.Information, eventId++);
        }

        public enum ServiceState
        {
            SERVICE_STOPPED = 0x00000001,
            SERVICE_START_PENDING = 0x00000002,
            SERVICE_STOP_PENDING = 0x00000003,
            SERVICE_RUNNING = 0x00000004,
            SERVICE_CONTINUE_PENDING = 0x00000005,
            SERVICE_PAUSE_PENDING = 0x00000006,
            SERVICE_PAUSED = 0x00000007,
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct ServiceStatus
        {
            public int dwServiceType;
            public ServiceState dwCurrentState;
            public int dwControlsAccepted;
            public int dwWin32ExitCode;
            public int dwServiceSpecificExitCode;
            public int dwCheckPoint;
            public int dwWaitHint;
        };

        [DllImport("advapi32.dll", SetLastError = true)]
        private static extern bool SetServiceStatus(IntPtr handle, ref ServiceStatus serviceStatus);

        private void InitializeComponent()
        {
            this.eventLog1 = new System.Diagnostics.EventLog();
            ((System.ComponentModel.ISupportInitialize)(this.eventLog1)).BeginInit();
            // 
            // iSkyService
            // 
            this.ServiceName = "iSkyService";
            ((System.ComponentModel.ISupportInitialize)(this.eventLog1)).EndInit();

        }

        public async void OnTimerAsync(object sender, System.Timers.ElapsedEventArgs args)
        {
            if (running) return;

            running = true;

            timer.Stop();

            //if (Settings.GetString("ConsentEnvironment") == "UAT")
            Thread.Sleep(15000);

            now = DateTime.Now;
            
            Dictionary<string, List<string>> runTypes = JsonConvert.DeserializeObject<Dictionary<string, List<string>>>(Settings.GetString("RunTypes"));
            List<iSkyRecord[]> results = new List<iSkyRecord[]>();
            
            foreach (KeyValuePair<string, List<string>> runType in runTypes)
            {
                int processed = 0;
                foreach (string tt in runType.Value)
                {
                    processed++;
            
                    eventLog1.WriteEntry(string.Format("Processing started for {0}_{1}", runType.Key, tt), EventLogEntryType.Information, eventId++);
                    results.Add(await ProcessiSkyRecords(runType.Key, tt));
                    eventLog1.WriteEntry(string.Format("Processing finished for {0}_{1}", runType.Key, tt), EventLogEntryType.Information, eventId++);
                }
            }
            
            int index = 0;
            foreach (KeyValuePair<string, List<string>> runType in runTypes)
            {
                foreach (string tt in runType.Value)
                {
                    eventLog1.WriteEntry(string.Format("Producing file for {0}_{1}", runType.Key, tt), EventLogEntryType.Information, eventId++);
                    OutputToFile(results[index++], runType.Key, tt);
                    eventLog1.WriteEntry(string.Format("Finished producing file for {0}_{1}", runType.Key, tt), EventLogEntryType.Information, eventId++);
                }
            }
            
            bool allFtp = true;
            
            if (Settings.GetBool("FTPUpload"))
            {
                foreach (KeyValuePair<string, List<string>> runType in runTypes)
                {
                    foreach (string tt in runType.Value)
                    {
                        string filename = GetFileName(runType.Key, tt);
            
                        eventLog1.WriteEntry(string.Format("Attempting to upload {0} to FTP", filename), EventLogEntryType.Information, eventId++);
            
                        if (FTPUpload(string.Format("{0}\\{1}", exportLocation, filename), runType.Key))
                            eventLog1.WriteEntry(string.Format("{0} uploaded to FTP", filename), EventLogEntryType.Information, eventId++);
                        else
                        {
                            allFtp = false;
                            eventLog1.WriteEntry(string.Format("{0} failed to upload to FTP", filename), EventLogEntryType.Warning, eventId++);
                        }
                    }
                }
            }
            else
            {
                allFtp = false;
                eventLog1.WriteEntry("FTP Upload is off!", EventLogEntryType.Information, eventId++);
            }

            if (QubeUtils.Email.Send(
                "noreply@toyotavaluechain.net",
                Settings.GetString("EmailRecipients").Split(',').ToList(),
                string.Format("iSky Complete - {0}", DateTime.Now.ToString("dd/MM/yyyy")),
                string.Format("iSky has completed. Please check <a href=\"https://toyotavaluechain.net/ISkyDailyReport.aspx?StartDate={0}&EndDate={0}\">here</a> for stats.<br/>{1}", DateTime.Now.ToString("yyyy-MM-dd"), Settings.GetBool("FTPUpload") ? (allFtp ? "All files uploaded to FTP." : "Files NOT uploaded to FTP.") : "FTP upload is turned off!"),
                true,
                "smtp.sendgrid.net",
                "apikey",
                "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"
                )
            )
                eventLog1.WriteEntry("Email Sent!", EventLogEntryType.Information, eventId++);
            else
                eventLog1.WriteEntry("Failed to send success email!", EventLogEntryType.Warning, eventId++);

            // When complete
            StartTimer();

            running = false;
        }

        private void StartTimer(bool startNow = false)
        {
            DateTime now = DateTime.Now;
            DateTime nextRun = DateTime.Parse(string.Format("{0} {1}", DateTime.Now.AddDays(1).ToString("yyyy-MM-dd"), Settings.GetString("StartTime")));
            TimeSpan timeLeft = nextRun - now;

            eventLog1.WriteEntry(string.Format("{0} Hour(s) {1} Minute(s) {2} Second(s) until next run.", timeLeft.Hours, timeLeft.Minutes, timeLeft.Seconds), EventLogEntryType.Information, eventId++);

            if (startNow)
                eventLog1.WriteEntry("Timer overridden. Starting now!", EventLogEntryType.Information, eventId++);

            timer.Interval = startNow ? 1 : timeLeft.TotalMilliseconds;
            timer.Elapsed += new System.Timers.ElapsedEventHandler(this.OnTimerAsync);
            timer.Start();
        }

        private async Task<iSkyRecord[]> ProcessiSkyRecords(string manufacturer, string transactionType)
        {
            List<iSkyRecord> toProcess = new List<iSkyRecord>();
            int maxRecords = Settings.GetInt("MaxRecords");

            //Add 50% to maxRecords to try to send 5000
            maxRecords += (int)Math.Ceiling(maxRecords * Settings.GetDecimal("ToProcessModifier"));

            toProcess = MSSQLHelper.GetList<iSkyRecord>("p_InvoiceList_iSKY" + (manufacturer == "LEXS" ? "_Lexus" : "") + " @dStartDate, @dEndDate, @sTransactionType", new SqlParameter[] {
                new SqlParameter("@dStartDate", SqlDbType.NVarChar) { Value = "" },
                new SqlParameter("@dEndDate", SqlDbType.NVarChar) { Value = "" },
                new SqlParameter("@sTransactionType", SqlDbType.NVarChar) { Value = transactionType },
            }, connStrName, 0);

            eventLog1.WriteEntry(string.Format("{0} Records to process", toProcess.Count()), EventLogEntryType.Information, eventId++);

            toProcess = toProcess.Take(maxRecords).ToList();

            if (Settings.GetBool("UploadCustomerData"))
                await UploadiSkyRecords(toProcess, transactionType, manufacturer);

            return await Task.WhenAll(toProcess.Select(a => ProcessiSkyRecord(a, transactionType, manufacturer)));
        }

        private async Task<bool> UploadiSkyRecords(List<iSkyRecord> records, string transactionType, string manufacturer)
        {
            Thread.Sleep(200);
            await throttler.WaitAsync();

            List<Task<bool>> res = new List<Task<bool>>();
            List<CustomerUploadRequest> customers = new List<CustomerUploadRequest>();

            eventLog1.WriteEntry(string.Format("{0}: {1} Uploading iSky Records", manufacturer, transactionType), EventLogEntryType.Information, eventId++);

            foreach (iSkyRecord rec in records)
            {
                customers.Add(
                    new CustomerUploadRequest()
                    {
                        marqueCode = manufacturer,
                        source = new Source()
                        {
                            code = "D0" + rec.DealerCode,
                            uniqueIdentifier = rec.CustomerMagicNumber
                        },
                        @event = new Event
                        {
                            code = (transactionType == "V" ? "VEHICLE_ORDER_NEW" : (transactionType == "U" ? "VEHICLE_ORDER_USED" : "INVOICE_SERVICE")),
                            //VEHICLE_ORDER_NEW - V
                            //VEHICLE_ORDER_USED - U??
                            //INVOICE_WARRANTY
                            //INVOICE_SERVICE - P
                            //INVOICE_PARTS
                            date = rec.InvoiceDate.ToString("yyyy-MM-dd")
                        },
                        person = new Person()
                        {
                            prefix = rec.Title,
                            initial = rec.Initials,
                            firstname = rec.FirstName,
                            surname = rec.Surname,
                            contact = new Contact()
                            {
                                address = new List<Address>()
                                {
                                    new Address()
                                    {
                                        type = "HOME",
                                        line1 = rec.Address1,
                                        locality = rec.Address2 + (string.IsNullOrEmpty(rec.Address3) ? string.Empty : ", " + rec.Address3),
                                        town = rec.Address4,
                                        county = rec.Address5,
                                        postcode = rec.PostCode,
                                        preferred = true
                                    }
                                },
                                email = new List<QubeUtils.WebApi.ConsentApi.Email>(),
                                telephone = new List<Telephone>()
                            },
                        },
                        vehicle = new List<Vehicle>()
                        {
                            new Vehicle()
                            {
                                vrm = rec.RegNumber,
                                vin = rec.ChassisNumber
                            }
                        }
                    }
                );

                if (!string.IsNullOrEmpty(rec.EmailAddress))
                    customers.Last().person.contact.email.Add(
                        new QubeUtils.WebApi.ConsentApi.Email()
                        {
                            type = "HOME",
                            address = rec.EmailAddress,
                            preferred = true
                        });

                if (!string.IsNullOrEmpty(rec.HomeTelephoneNumber))
                    customers.Last().person.contact.telephone.Add(
                        new Telephone()
                        {
                            type = "HOME",
                            number = rec.HomeTelephoneNumber,
                            preferred = (string.IsNullOrEmpty(rec.DayTelephoneNumber) && string.IsNullOrEmpty(rec.MobileNumber))
                        });

                if (!string.IsNullOrEmpty(rec.DayTelephoneNumber))
                    customers.Last().person.contact.telephone.Add(
                        new Telephone()
                        {
                            type = "WORK",
                            number = rec.DayTelephoneNumber,
                            preferred = (string.IsNullOrEmpty(rec.HomeTelephoneNumber) && string.IsNullOrEmpty(rec.MobileNumber))
                        });

                if (!string.IsNullOrEmpty(rec.MobileNumber))
                    customers.Last().person.contact.telephone.Add(
                        new Telephone()
                        {
                            type = "HOME",
                            number = rec.MobileNumber,
                            preferred = !string.IsNullOrEmpty(rec.MobileNumber)
                        });
            }

            foreach (CustomerUploadRequest customer in customers)
            {

                Task<bool> task = MarketingConsent.UploadCustomer(customer, manufacturer, transactionType);
                res.Add(task);
            }

            foreach (Task<bool> task in res)
            {
                try
                {
                    await task;
                }
                catch (AggregateException ae)
                {
                    ae.Handle((e) => false);
                }
                finally
                {
                    throttler.Release();
                }
            }

            if (res.Any(a => a.Result == false))
                return false;

            return true;
        }

        private async Task<iSkyRecord> ProcessiSkyRecord(iSkyRecord record, string transactionType, string manufacturer)
        {
            Thread.Sleep(200);
            await throttler.WaitAsync();

            eventLog1.WriteEntry(string.Format("{0}: {1} begin processing", record.DealerCode, record.CustomerMagicNumber), EventLogEntryType.Information, eventId++);

            try
            {
                Task<bool> task = MarketingConsent.CheckAsync(new ConsentApiRequestEntity(
                    record.Surname,
                    record.FirstName,
                    record.Title,
                    record.Initials,
                    new List<string> { record.EmailAddress },
                    new List<string> { record.HomeTelephoneNumber, record.DayTelephoneNumber, record.MobileNumber },
                    record.PostCode,
                    record.Address1
                ), record.DealerCode + ": " + record.CustomerMagicNumber.ToString(), "Surveys", transactionType == "P" ? "Aftersales" : "Sales", record.Manufacturer == "Toyota" ? "TOYT" : "LEXS");

                bool res = false;

                try
                {
                    res = await task;
                }
                catch (AggregateException ae)
                {
                    ae.Handle((e) => true);
                }
                finally
                {
                    record.Consent = task.Status == TaskStatus.RanToCompletion ? res : false;
                }
            }
            finally
            {
                throttler.Release();
            }

            return record;
        }

        //private iSkyRecord iSkyReadToObject(SqlDataReader dr)
        //{
        //    return new iSkyRecord
        //    {
        //        DealerCode = dr.IsDBNull(0) ? string.Empty : dr.GetString(0),
        //        CentreName = dr.IsDBNull(1) ? string.Empty : dr.GetString(1),
        //        CentreAddress1 = dr.IsDBNull(2) ? string.Empty : dr.GetString(2),
        //        CentreAddress4 = dr.IsDBNull(3) ? string.Empty : dr.GetString(3),
        //        TransactionType = dr.IsDBNull(4) ? string.Empty : dr.GetString(4),
        //        InvoiceDate = dr.IsDBNull(5) ? new DateTime(1900, 1, 1) : dr.GetDateTime(5),
        //        CustomerMagicNumber = dr.IsDBNull(6) ? string.Empty : dr.GetString(6),
        //        Title = dr.IsDBNull(7) ? string.Empty : dr.GetString(7),
        //        Initials = dr.IsDBNull(8) ? string.Empty : dr.GetString(8),
        //        FirstName = dr.IsDBNull(9) ? string.Empty : dr.GetString(9),
        //        Surname = dr.IsDBNull(10) ? string.Empty : dr.GetString(10),
        //        EmailAddress = dr.IsDBNull(11) ? string.Empty : dr.GetString(11),
        //        Address1 = dr.IsDBNull(12) ? string.Empty : dr.GetString(12),
        //        Address2 = dr.IsDBNull(13) ? string.Empty : dr.GetString(13),
        //        Address3 = dr.IsDBNull(14) ? string.Empty : dr.GetString(14),
        //        Address4 = dr.IsDBNull(15) ? string.Empty : dr.GetString(15),
        //        Address5 = dr.IsDBNull(16) ? string.Empty : dr.GetString(16),
        //        PostCode = dr.IsDBNull(17) ? string.Empty : dr.GetString(17),
        //        HomeTelephoneNumber = dr.IsDBNull(18) ? string.Empty : dr.GetString(18),
        //        DayTelephoneNumber = dr.IsDBNull(19) ? string.Empty : dr.GetString(19),
        //        MobileNumber = dr.IsDBNull(20) ? string.Empty : dr.GetString(20),
        //        FaxNumber = dr.IsDBNull(21) ? string.Empty : dr.GetString(21),
        //        CompanyName = dr.IsDBNull(22) ? string.Empty : dr.GetString(22),
        //        CompanyAddress1 = dr.IsDBNull(23) ? string.Empty : dr.GetString(23),
        //        CompanyAddress2 = dr.IsDBNull(24) ? string.Empty : dr.GetString(24),
        //        CompanyAddress3 = dr.IsDBNull(25) ? string.Empty : dr.GetString(25),
        //        CompanyAddress4 = dr.IsDBNull(26) ? string.Empty : dr.GetString(26),
        //        CompanyAddress5 = dr.IsDBNull(27) ? string.Empty : dr.GetString(27),
        //        CompanyPostCode = dr.IsDBNull(28) ? string.Empty : dr.GetString(28),
        //        StandardModel = dr.IsDBNull(29) ? string.Empty : dr.GetString(29),
        //        RegNumber = dr.IsDBNull(30) ? string.Empty : dr.GetString(30),
        //        ChassisNumber = dr.IsDBNull(31) ? string.Empty : dr.GetString(31),
        //        RangeName = dr.IsDBNull(32) ? string.Empty : dr.GetString(32),
        //        KatashikiModel = dr.IsDBNull(33) ? string.Empty : dr.GetString(33),
        //        RegDate = dr.IsDBNull(34) ? new DateTime(1900, 1, 1) : dr.GetDateTime(34),
        //        iSKYModelId = dr.IsDBNull(35) ? string.Empty : dr[35].ToString(),
        //        TotalInvoiceValue = dr.IsDBNull(36) ? 0m : dr.GetDecimal(36),
        //        APIRan = dr.GetBoolean(37),
        //        Mileage = dr.IsDBNull(38) ? 0 : int.Parse(dr.GetString(38)),
        //        HasWarranty = dr.GetBoolean(39),
        //    };
        //}

        private int GetISkyBacklog(string manufacturer, string transactionType)
        {
            List<iSkyRecord> toProcess = MSSQLHelper.GetList<iSkyRecord>("p_InvoiceList_iSKY" + (manufacturer == "LEXS" ? "_Lexus" : "") + " @dStartDate, @dEndDate, @sTransactionType", new SqlParameter[] {
                new SqlParameter("@dStartDate", SqlDbType.NVarChar) { Value = "" },
                new SqlParameter("@dEndDate", SqlDbType.NVarChar) { Value = "" },
                new SqlParameter("@sTransactionType", SqlDbType.NVarChar) { Value = transactionType },
            }, connStrName, 0);

            return toProcess.Where(w => !w.APIRan).Count();
        }

        private string GetFileName(string manufacturer, string transactionType)
        {
            if (manufacturer != "ALL")
            {
                return string.Format("{0}_{1}_{2}.xml", manufacturer == "TOYT" ? "TOY" : "LEX", now.ToString("yyyyMMdd"), transactionType);
            } else if (transactionType == "U") {
                return string.Format("FVOCSPS_VOC44000_94081_VOCPRD_CLUC_{0:yyyyMMdd}_{1:HHmmss}.xml", now, now);
            } else if (transactionType == "F") {
                return string.Format("FVOCSPS_VOC44000_94081_VOCPRD_CL_{0:yyyyMMdd}_{1:HHmmss}.xml", now, now);
            } else {
                return string.Format("{0}_{1}_{2}.xml", manufacturer, now.ToString("yyyyMMdd"), transactionType);
            }
        }

        private void OutputToFile(iSkyRecord[] results, string manufacturer, string transactionType)
        {
            int count = results.Count();

            results = results.Where(w => w.Consent).Select(s => s).ToArray();

            int countConsented = results.Count();

            string fileName = GetFileName(manufacturer, transactionType);

            int exportId = 0;

            exportId = int.Parse(MSSQLHelper.ExecuteScalar("INSERT INTO iSKYExportControl(CreatedDate, FileName, TransactionType, Count, CountConsented, Backlog, Visible) VALUES(@CreatedDate, @Filename, @TransactionType, @Count, @CountConsented, @Backlog, @Visible); SELECT @@IDENTITY;", new SqlParameter[] {
                new SqlParameter("@CreatedDate", SqlDbType.DateTime) {Value = DateTime.Now.ToString("yyyy-MM-dd")},
                new SqlParameter("@Filename", SqlDbType.VarChar) {Value = fileName},
                new SqlParameter("@TransactionType", SqlDbType.VarChar) {Value = transactionType},
                new SqlParameter("@Count", SqlDbType.Int) {Value = count},
                new SqlParameter("@CountConsented", SqlDbType.Int) {Value = countConsented},
                new SqlParameter("@Backlog", SqlDbType.Int) { Value = GetISkyBacklog(manufacturer, transactionType) },
                new SqlParameter("@Visible", SqlDbType.Bit) { Value = Settings.GetBool("UpdateControl") }
            }, connStrName).ToString());

            bool isOldFormat = new List<string> { "V", "P" }.Contains(transactionType);

            XmlWriterSettings oWriterSettings = new XmlWriterSettings
            {
                CheckCharacters = true,
                Encoding = isOldFormat ? System.Text.Encoding.UTF8 : System.Text.Encoding.Unicode,
                Indent = true
            };

            XmlWriter oWriter = XmlWriter.Create(string.Format("{0}\\{1}", exportLocation, fileName), oWriterSettings);
            oWriter.WriteStartDocument();

            if (isOldFormat)
                oWriter.WriteStartElement("Interviews");
            else
                oWriter.WriteStartElement("root");

            if (isOldFormat)
            {
                oWriter.WriteElementString("SuppliedBy", "qubeDATA");
                oWriter.WriteElementString("ExtractDate", DateTime.Now.ToString("yyyy-MM-dd"));
            }

            foreach (iSkyRecord res in results.OrderBy(o => o.InvoiceDate).Take(Settings.GetInt("MaxRecords")).ToArray())
            {
                if (Settings.GetString("ConsentEnvironment") != "UAT")
                {
                    MSSQLHelper.ExecuteSQL("INSERT INTO iSKYExportInvoices(iSKYExportId, InvoiceDate, DealerCode, CustomerMagicNumber) VALUES(@ExportId, @InvoiceDate, @DealerCode, @MagicNumber)", new SqlParameter[] {
                        new SqlParameter("@ExportId", SqlDbType.Int) {Value = exportId},
                        new SqlParameter("@InvoiceDate", SqlDbType.DateTime) {Value = res.InvoiceDate},
                        new SqlParameter("@DealerCode", SqlDbType.VarChar) {Value = res.DealerCode},
                        new SqlParameter("@MagicNumber", SqlDbType.VarChar) {Value = res.CustomerMagicNumber}
                    }, connStrName);
                }

                if(isOldFormat)
                    OutputFileOld(oWriter, res, manufacturer);
                else
                    OutputFileNew(oWriter, res, manufacturer, transactionType);
            }
            
            oWriter.WriteEndDocument();
            oWriter.Flush();
            oWriter.Close();
        }

        private void OutputFileNew(XmlWriter oWriter, iSkyRecord res, string manufacturer, string transactionType, bool isOldFormat = false)
        {
            oWriter.WriteStartElement("Interview");

            if(isOldFormat)
                oWriter.WriteElementString("orgUnitCode", string.Format("{0:00000}", res.DealerCode));
            else
                oWriter.WriteElementString("orgUnitCode", "");

            if (isOldFormat)
                oWriter.WriteElementString("orgUnitType", "1");
            oWriter.WriteElementString("orgUnitName1", FormatOutput(res.CentreName, 100));
            oWriter.WriteElementString("orgUnitName2", "");

            if (isOldFormat)
            {
                oWriter.WriteElementString("orgUnitStreet", FormatOutput(res.CentreAddress1, 80));
                oWriter.WriteElementString("orgUnitId", "");
                oWriter.WriteElementString("tnsAddressID", "");
            }

            oWriter.WriteElementString("orgUnitCity", FormatOutput(res.CentreAddress4, 80));
            oWriter.WriteElementString("employeeCode", string.Format("{0:00000}", res.DealerCode));
            oWriter.WriteElementString("employeeName1", FormatOutput(res.CentreName, 80));
            oWriter.WriteElementString("employeeName2", FormatOutput(res.CentreName, 80));
            oWriter.WriteElementString("employeeType", "");
            oWriter.WriteElementString("employeeEmailAddress", FormatOutput(res.CentreEmail, 200));
            oWriter.WriteElementString("customerCode", "" + res.CustomerMagicNumber);

            if (isOldFormat)
            {
                oWriter.WriteElementString("customerorderNo", "");
                oWriter.WriteElementString("customerLang", "en-GB");
                oWriter.WriteElementString("customerType", "2");
                oWriter.WriteElementString("salesType", "");
                oWriter.WriteElementString("customercategory", "");
                oWriter.WriteElementString("customerstatus", "");
            }
            else
            {
                oWriter.WriteElementString("customerLang", "EN");
                oWriter.WriteElementString("customerType", "2");
            }

            oWriter.WriteElementString("customerName1", FormatOutput(res.Surname, 80));
            oWriter.WriteElementString("customerName2", FormatOutput(res.FirstName, 80));
            if (isOldFormat)
            {
                oWriter.WriteElementString("customerName3", "");
                oWriter.WriteElementString("customerName4", "");
            }

            if (isOldFormat)
                oWriter.WriteElementString("customerbirthdate", "");
            else
                oWriter.WriteElementString("customerBirthDate", "");

            oWriter.WriteElementString("customerTitle", FormatOutput("" + res.Title, 20));

            if (isOldFormat)
                oWriter.WriteElementString("customerOccupation", "");

            oWriter.WriteElementString("customerGender", "");
            oWriter.WriteElementString("customerSalutation", "");

            oWriter.WriteElementString("fleetSize", "");
            oWriter.WriteElementString("leasing", "");
            oWriter.WriteElementString("fleetIndustry", "");

            if (isOldFormat)
            {
                oWriter.WriteElementString("customerDescription", "");
                oWriter.WriteElementString("customerRemark", "");
            }

            oWriter.WriteElementString("preferredChannel", "2");

            oWriter.WriteElementString("addressPostal1", FormatOutput("" + res.Address1, 50));
            oWriter.WriteElementString("addressPostal2", FormatOutput("" + res.PostCode, 8));
            oWriter.WriteElementString("addressPostal3", FormatOutput("" + res.Address2, 50));
            oWriter.WriteElementString("postalCountry", "GB");

            if (isOldFormat)
            {
                if (manufacturer == "TOYT")
                {
                    oWriter.WriteElementString("phone1", "");
                    oWriter.WriteElementString("phone2", "");
                    oWriter.WriteElementString("phone3", FormatOutput(GetMobileNumber(res.HomeTelephoneNumber, res.DayTelephoneNumber, res.MobileNumber, res.FaxNumber), 20));
                    oWriter.WriteElementString("phone4", "");
                }
                else
                {
                    oWriter.WriteElementString("phone1", FormatOutput(res.HomeTelephoneNumber, 20));
                    oWriter.WriteElementString("phone2", FormatOutput(res.DayTelephoneNumber, 20));
                    oWriter.WriteElementString("phone3", FormatOutput(GetMobileNumber(res.MobileNumber, "", "", ""), 20));
                    oWriter.WriteElementString("phone4", FormatOutput(GetMobileNumber(res.FaxNumber, "", "", ""), 20));
                }
            }
            else
            {
                oWriter.WriteElementString("phonePersonal", FormatOutput(res.HomeTelephoneNumber, 20));
                oWriter.WriteElementString("phoneWork", FormatOutput(res.DayTelephoneNumber, 20));
                oWriter.WriteElementString("phoneMobile", FormatOutput(GetMobileNumber(res.MobileNumber, "", "", ""), 20));
                oWriter.WriteElementString("phoneFax", FormatOutput(GetMobileNumber(res.FaxNumber, "", "", ""), 20));
            }

            oWriter.WriteElementString("addressPostal4", FormatOutput("" + res.Address3, 50));
            oWriter.WriteElementString("addressPostal5", FormatOutput("" + res.Address4, 50));

            if(!isOldFormat)
                oWriter.WriteElementString("preferredPhone", GetPreferredPhone(new string[] { res.HomeTelephoneNumber, res.DayTelephoneNumber, res.MobileNumber, res.FaxNumber }).ToString());

            oWriter.WriteElementString("emailType1", FormatOutput("" + res.EmailAddress, 250));
            oWriter.WriteElementString("emailType2", "");
            oWriter.WriteElementString("preferredEmail", "1");
          

            oWriter.WriteElementString("productIdent", ("" + res.iSKYModelId == "") ? (manufacturer == "TOYT" ? "101" : "201") : ("" + res.iSKYModelId));
            oWriter.WriteElementString("productVIN", FormatOutput("" + res.ChassisNumber, 17));
            oWriter.WriteElementString("productDate", "" + res.RegDate.ToString("yyyy-MM-dd"));
            oWriter.WriteElementString("eventDate", "" + res.InvoiceDate.ToString("yyyy-MM-dd"));
            oWriter.WriteElementString("productName", "" + res.StandardModel);
            oWriter.WriteElementString("productInfo1", FormatOutput("" + res.RegNumber, 20));
            oWriter.WriteElementString("productBrand", res.Manufacturer);

            if(isOldFormat)
                oWriter.WriteElementString("dealerExternalCode", "");
            else
                oWriter.WriteElementString("dealerExternalCode", string.Format("{0:00000}", res.DealerCode));

            if (isOldFormat)
                oWriter.WriteElementString("productRemark", "");

            oWriter.WriteElementString("eventCode", "");
            if ("" + res.TransactionType == "P")
            {
                oWriter.WriteElementString("eventType", "3");
                oWriter.WriteElementString("repairType", "");
                oWriter.WriteElementString("repairCosts", string.Format("{0:0.00}", "" + res.TotalInvoiceValue));
            }
            else
            {
                oWriter.WriteElementString("eventType", "2");
                oWriter.WriteElementString("repairType", "");
                oWriter.WriteElementString("repairCosts", "0.00");
            }

            oWriter.WriteElementString("optInEmail", "");
            oWriter.WriteElementString("optInPhone", "");
            oWriter.WriteElementString("optInSMS", "");
            oWriter.WriteElementString("optInForSurvey", "");
            oWriter.WriteElementString("usageType", transactionType == "F" ? "2" : "1");
            oWriter.WriteElementString("salesType", "");

            oWriter.WriteElementString("addressBusinessName1", string.IsNullOrEmpty(FormatOutput("" + res.CompanyName, 50)) ? "abc" : FormatOutput("" + res.CompanyName, 50));
            oWriter.WriteElementString("addressBusinessName2", "");
            oWriter.WriteElementString("addressBusiness1", FormatOutput("" + res.CompanyAddress1, 50));
            oWriter.WriteElementString("addressBusiness2", FormatOutput("" + res.CompanyPostCode, 8));
            oWriter.WriteElementString("addressBusiness3", FormatOutput("" + res.CompanyAddress2, 50));
            oWriter.WriteElementString("addressBusiness4", FormatOutput("" + res.CompanyAddress3, 50));
            oWriter.WriteElementString("addressBusiness5", FormatOutput("" + res.CompanyAddress4, 50));
            oWriter.WriteElementString("addressBusinessCountry", "GB");


            if (isOldFormat)
            {
                oWriter.WriteElementString("TFS", "");
                oWriter.WriteElementString("Insurance", "");
                oWriter.WriteElementString("Diagnostic", "");
            }


            oWriter.WriteElementString("qQResponse", "");

            if (!isOldFormat)
            {
                oWriter.WriteElementString("retailerExperience", res.Manufacturer);
                oWriter.WriteElementString("certifiedCar", "Yes");
                oWriter.WriteElementString("mileage", res.Mileage.ToString());
                oWriter.WriteElementString("warranty", res.HasWarranty ? "Yes" : "No");

            }

            oWriter.WriteEndElement();
        }

        private void OutputFileOld(XmlWriter oWriter, iSkyRecord res, string manufacturer)
        {
            oWriter.WriteStartElement("Interview");
            oWriter.WriteElementString("orgUnitCode", string.Format("{0:00000}", res.DealerCode));
            oWriter.WriteElementString("orgUnitType", "1");
            oWriter.WriteElementString("orgUnitName1", FormatOutput(res.CentreName, 100));
            oWriter.WriteElementString("orgUnitName2", "");
            oWriter.WriteElementString("orgUnitStreet", FormatOutput(res.CentreAddress1, 80));
            oWriter.WriteElementString("orgUnitCity", FormatOutput(res.CentreAddress4, 80));
            oWriter.WriteElementString("employeeCode", "");
            oWriter.WriteElementString("employeeType", "");
            oWriter.WriteElementString("employeeName1", "");
            oWriter.WriteElementString("employeeName2", "");
            oWriter.WriteElementString("orgUnitId", "");
            oWriter.WriteElementString("tnsAddressID", "");
            oWriter.WriteElementString("customerCode", "" + res.CustomerMagicNumber);
            oWriter.WriteElementString("customerorderNo", "");
            oWriter.WriteElementString("customerLang", "en-GB");
            oWriter.WriteElementString("customerbirthdate", "");
            oWriter.WriteElementString("customerType", "2");
            oWriter.WriteElementString("usageType", "");
            oWriter.WriteElementString("salesType", "");
            oWriter.WriteElementString("customercategory", "");
            oWriter.WriteElementString("customerstatus", "");

            oWriter.WriteElementString("customerName1", FormatOutput(res.Surname, 80));
            oWriter.WriteElementString("customerName2", FormatOutput(res.FirstName, 80));
            oWriter.WriteElementString("customerName3", "");
            oWriter.WriteElementString("customerName4", "");

            oWriter.WriteElementString("customerTitle", FormatOutput("" + res.Title, 20));

            oWriter.WriteElementString("customerOccupation", "");
            oWriter.WriteElementString("customerGender", "");
            oWriter.WriteElementString("customerSalutation", "");
            oWriter.WriteElementString("customerDescription", "");
            oWriter.WriteElementString("customerRemark", "");
            oWriter.WriteElementString("preferredChannel", "2");

            oWriter.WriteElementString("addressPostal1", FormatOutput("" + res.Address1, 50));
            oWriter.WriteElementString("addressPostal2", FormatOutput("" + res.PostCode, 8));
            oWriter.WriteElementString("addressPostal3", FormatOutput("" + res.Address2, 50));
            oWriter.WriteElementString("addressPostal4", FormatOutput("" + res.Address3, 50));
            oWriter.WriteElementString("addressPostal5", FormatOutput("" + res.Address4, 50));
            oWriter.WriteElementString("postalCountry", "GB");

            oWriter.WriteElementString("addressBusinessName1", FormatOutput("" + res.CompanyName, 50));
            oWriter.WriteElementString("addressBusinessName2", "");
            oWriter.WriteElementString("addressBusiness1", FormatOutput("" + res.CompanyAddress1, 50));
            oWriter.WriteElementString("addressBusiness2", FormatOutput("" + res.CompanyPostCode, 8));
            oWriter.WriteElementString("addressBusiness3", FormatOutput("" + res.CompanyAddress2, 50));
            oWriter.WriteElementString("addressBusiness4", FormatOutput("" + res.CompanyAddress3, 50));
            oWriter.WriteElementString("addressBusiness5", FormatOutput("" + res.CompanyAddress4, 50));
            oWriter.WriteElementString("addressBusinessCountry", "GB");

            if (manufacturer == "TOYT")
            {
                oWriter.WriteElementString("phone1", "");
                oWriter.WriteElementString("phone2", "");
                oWriter.WriteElementString("phone3", FormatOutput(GetMobileNumber(res.HomeTelephoneNumber, res.DayTelephoneNumber, res.MobileNumber, res.FaxNumber), 20));
                oWriter.WriteElementString("phone4", "");
            }
            else
            {
                oWriter.WriteElementString("phone1", FormatOutput(res.HomeTelephoneNumber, 20));
                oWriter.WriteElementString("phone2", FormatOutput(res.DayTelephoneNumber, 20));
                oWriter.WriteElementString("phone3", FormatOutput(GetMobileNumber(res.MobileNumber, "", "", ""), 20));
                oWriter.WriteElementString("phone4", FormatOutput(GetMobileNumber(res.FaxNumber, "", "", ""), 20));
            }

            oWriter.WriteElementString("preferredPhone", "3");

            oWriter.WriteElementString("emailType1", FormatOutput("" + res.EmailAddress, 250));
            oWriter.WriteElementString("emailType2", "");
            oWriter.WriteElementString("preferredEmail", "1");

            oWriter.WriteElementString("productIdent", ("" + res.iSKYModelId == "") ? (manufacturer == "TOYT" ? "101" : "201") : ("" + res.iSKYModelId));
            oWriter.WriteElementString("productVIN", FormatOutput("" + res.ChassisNumber, 17));
            oWriter.WriteElementString("productDate", "" + res.RegDate.ToString("yyyy-MM-dd"));
            oWriter.WriteElementString("eventDate", "" + res.InvoiceDate.ToString("yyyy-MM-dd"));
            oWriter.WriteElementString("productName", "" + res.StandardModel);
            oWriter.WriteElementString("productInfo1", FormatOutput("" + res.RegNumber, 20));
            oWriter.WriteElementString("productInfo2", ""); /// Could be Katashiki Code in the future.
            oWriter.WriteElementString("productBrand", manufacturer == "TOYT" ? "Toyota" : "Lexus");
            oWriter.WriteElementString("productRemark", "");
            oWriter.WriteElementString("eventCode", "");
            if ("" + res.TransactionType == "P")
            {
                oWriter.WriteElementString("eventType", "3");
                oWriter.WriteElementString("repairCosts", string.Format("{0:0.00}", "" + res.TotalInvoiceValue));
            }
            else
            {
                oWriter.WriteElementString("eventType", "2");
                oWriter.WriteElementString("repairCosts", "0.00");
            }
            oWriter.WriteElementString("TFS", "");
            oWriter.WriteElementString("Insurance", "");
            oWriter.WriteElementString("Diagnostic", "");
            oWriter.WriteElementString("repairType", "");
            if (manufacturer != "TOYT")
                oWriter.WriteElementString("QQResponse", "");
            oWriter.WriteEndElement();
        }

        private int GetPreferredPhone(string[] vs)
        {
            int returnIndex = 1;

            foreach(string s in vs)
            {
                if (string.IsNullOrEmpty(s.Trim()))
                    returnIndex++;
                else
                    break;
            }

            return returnIndex;
        }

        private string FormatOutput(string sStr, int nLength)
        {
            string returnValue = "";

            int i = 0;
            string sLetters = "!#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~ ";
            string sChar = "";
            string sNewString = "";

            try
            {
                sStr = sStr.Trim();
                for (i = 1; i <= sStr.Length; i++)
                {
                    sChar = sStr.Substring(i - 1, 1);
                    if (sLetters.IndexOf(sChar) + 1 > 0)
                    {
                        sNewString = sNewString + sChar;
                    }
                }
                if (sNewString.Length > nLength)
                {
                    returnValue = System.Convert.ToString(sNewString.Substring(0, nLength));
                }
                else
                {
                    returnValue = sNewString;
                }
            }
            catch (Exception)
            {
                returnValue = "";
            }

            return returnValue;
        }

        private string GetMobileNumber(string sPhone1, string sPhone2, string sPhone3, string sPhone4)
        {
            string returnValue = "";

            sPhone1 = GeneralUtilities.StripAllButNumbers(sPhone1);
            sPhone2 = GeneralUtilities.StripAllButNumbers(sPhone2);
            sPhone3 = GeneralUtilities.StripAllButNumbers(sPhone3);
            sPhone4 = GeneralUtilities.StripAllButNumbers(sPhone4);

            if (GeneralUtilities.IsMobileNumber(sPhone1))
            {
                returnValue = sPhone1;
            }
            else if (GeneralUtilities.IsMobileNumber(sPhone2))
            {
                returnValue = sPhone2;
            }
            else if (GeneralUtilities.IsMobileNumber(sPhone3))
            {
                returnValue = sPhone3;
            }
            else if (GeneralUtilities.IsMobileNumber(sPhone4))
            {
                returnValue = sPhone4;
            }
            else
            {
                returnValue = "";
            }

            return returnValue;
        }

        private bool FTPUpload(string file, string manufacturer)
        {
            if (file == null) throw new ArgumentNullException("file");
            if (file.IndexOfAny(Path.GetInvalidPathChars()) > -1) throw new ArgumentOutOfRangeException("Invalid file path");
            if (manufacturer == null) throw new ArgumentNullException("manufacturer");
            if (!new List<string> { "TOYT", "LEXS", "ALL" }.Contains(manufacturer)) throw new ArgumentOutOfRangeException("Invalid manufacturer");
            
            string ftpType = manufacturer == "TOYT" ? "Toy" : (manufacturer == "LEXS" ? "Lex" : "All");

            string url = Settings.GetString("FTP" + ftpType + "Url");
            string username = Settings.GetString("FTP" + ftpType + "User");
            string password =  Settings.GetString("FTP" + ftpType + "Pass");
            string keyFilename = manufacturer == "TOYT" || manufacturer == "LEXS" ?  Settings.GetString("FTP" + ftpType + "Key") : "";

            FTP ftp = new FTP(url, username, password, 22, keyFilename);

            return ftp.Upload(file, Settings.GetString("FTP" + ftpType + "RemotePath"));
        }
    }
}
